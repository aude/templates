## Description

**What is the feature?**

**Who will use this new feature?**

**Why these users would like to use this feature?**

<Define start and due date>

/label ~
<Assign label: priority, scope, type>

## Definition of Ready (DoR)

- [ ] Epic description properly filled
    - including labels (priority, scope, type), start and due date
- [ ] 80% issues ~"stage::awaiting development", and 100% of the ~"priority::P1" 
    - [ ] Description, including test cases, DOR (collapsible) and DoD (collapsible)
    - [ ] Milestone set
    - [ ] Labels: priority, scope, type
- [ ] Design ready
- [ ] 100% studies issues done
    - studies should include architecture, and side effect analysis 
- [ ] Planification with the technical team 

## Definition of Done (DoD)

- [ ] Deliverables
- [ ] Documentation
- [ ] Translation
- [ ] Communication
- [ ] License
- [ ] Test session
- [ ] 100% in ~"stage::awaiting deployment" or closed


