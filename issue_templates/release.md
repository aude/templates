# 📃 Tasks

* [ ] Release notes
* [ ] Validation of <beta milestone>
* [ ] Run dev builds about <beta milestone>
* [ ] Share <beta milestone> with testing community
* [ ] Validation of <rc milestone>
* [ ] Validation of <update milestone>

# 📆 Release Plan

- [ ] Publish release notes

## day n

- [ ] Update the App Lounge APK at https://doc.e.foundation/support-topics/app_lounge#download-app-lounge-apks
- [ ] Publish ~microG EN
- [ ] Update the ~one link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/one/one-en
- [ ] Release Documentation
- [ ] Open download for new devices
- [ ] Open download for device upgrade
- [ ] Update ~FP4 [documentation](https://doc.e.foundation/devices/FP4/install#downloads-for-the-fp4) if security patch level has changed
- [ ] 25% availability of ~s dev builds
- [ ] 25% availability of ~r dev builds
- [ ] 25% availability of ~q dev builds
- [ ] Share the release on `/e/OS - Murena team` Telegram channel

## day n + 4

- [ ] 100% availability of ~s dev builds
- [ ] 100% availability of ~r dev builds
- [ ] 100% availability of ~q dev builds
- [ ] 25% availability of ~s stable builds
- [ ] 25% availability of ~r stable builds
- [ ] 25% availability of ~q stable builds

## day n + 8

- [ ] 100% availability of ~s stable builds
- [ ] 100% availability of ~r stable builds
- [ ] 100% availability of ~q stable builds
- [ ] Update the ~one link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/one/one-en
- [ ] Update the ~FP4 link at https://gitlab.e.foundation/ecorp/flash-station-doc/-/wikis/fp4/fp4-edl-qfil-en
- [ ] Inform ESPRI

/label ~"type::Task"
/milestone %
