- /e/ version:
- Device model(s):
- Developer mode enabled: yes/no
- Device rooted: yes/no
- Trackers blocker enabled: yes/no

## Summary

<Summarize the bug encountered briefly and precisely>

## The problem

**Steps to reproduce**

<How one can reproduce the issue>

**What is the current behavior?**

<What actually happens>

**What is the expected correct behavior?**

<What you should see instead>

## Technical informations

**Relevant logs (`adb logcat`)**

<Paste any relevant logs in the codeblock bellow>

```

```

**Relevant screenshots**

<Screenshots of the problem>

## Solutions

**Workaround**

<To get the feature working or at least to make the device usable>

**Possible fixes**

<Any idea to fix the issue or a link to the line of code that might be the cause for this problem>

/label ~"type::Bug"
<Add the labels corresponding to your issue by adding a tilde and typing the name of the label you think apply to your issue in the line above. You need to type a tilde before the name of each label you want to apply to the issue.>
