## Summary

<Summarize the feature briefly and precisely>

## Description

**What is the feature?**

<Describe the feature in detail>

**Who will use this new feature?**

<Describe the kind of users who want this feature>

**Why these users would like to use this feature?**

<Describe the reason why the users would appreciate this feature and what it would bring to them.>

## Examples

<Give the example of what users will be able to accomplish with the feature>

## Reflection

**Mockups**

**Diagrams**

## Validation

<List test case that will be run to validate that the issue is working as expected>

/label ~"type::Feature Proposal"
<Add the labels corresponding to your issue by adding a tilde and typing the name of the label you think apply to your issue in the line above. You need to type a tilde before the name of each label you want to apply to the issue.>
