## Description

<!-- Give a description of what you are trying to implement from a user/high-level perspective so that the reviewer has enough context to start the review of your code.-->

## Screenshots

<!-- Provide screenshots so that the reviewer has some visual context. -->

## Technical details

<!-- Technical details should go in the description of the commit so you should be able to remove this part here. -->

## Tests

<!-- Explain here the test scenarios that have been done for the feature, including corner cases. -->

## Issues

<!-- Add a link to the gitlab issue to keep track of MR. -->

## 10 commandments of code reviews

<!-- Keep the link below to spread some love. -->

:family: :heart: https://gitlab.e.foundation/internal/wiki/-/wikis/development/code-review
